package com.saturn.springcache.service;

import com.saturn.springcache.model.Order;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    public Optional<Order> getOrder(String id);
    public Order putOrder(Order order);
    public Order updateOrder(Order order);
    public void deleteOrder(String id);
    public List<Order> getOrders();
}
