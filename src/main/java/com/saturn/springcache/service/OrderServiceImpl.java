package com.saturn.springcache.service;

import com.saturn.springcache.model.Order;
import com.saturn.springcache.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Optional<Order> getOrder(String id) {
        Optional<Order> order = orderRepository.findById(id);
        return order;
    }

    @Override
    @CachePut(value = "orders")
    public Order putOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    @CachePut(value = "orders", key = "#orderId")
    public Order updateOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    @CachePut(value = "orders", key = "#orderId")
    public void deleteOrder(String orderId) {
        orderRepository.deleteById(orderId);
    }

    @Override
    @Cacheable("orders")
    public List<Order> getOrders() {
        return orderRepository.findAll();
    }
}
