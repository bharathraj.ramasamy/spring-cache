package com.saturn.springcache.config;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfig {

    private static HazelcastInstance hazelcastInstance;

    public static HazelcastInstance getHazelcastInstance(){
        if(hazelcastInstance == null){
            hazelcastInstance = Hazelcast.newHazelcastInstance(getConfig());
        }
        return hazelcastInstance;
    }

    private static Config getConfig() {

        Config config = new Config();
        config.setInstanceName("scalper-instance");
        return  config;
    }

}
