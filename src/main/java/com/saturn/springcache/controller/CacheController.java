package com.saturn.springcache.controller;

import com.saturn.springcache.model.Order;
import com.saturn.springcache.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SuppressWarnings("ALL")
@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public Order getOrder(@RequestParam String orderId){
        if(orderService.getOrder(orderId).isPresent()){
            return orderService.getOrder(orderId).get();
        }
        return null;
    }

    @PostMapping(value = "/putOrder")
    public ResponseEntity<Order> putOrder(@RequestBody Order order){
        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.putOrder(order));
    }

    @PutMapping(value = "/updateOrder", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> updateOrder(@RequestBody Order order){
        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.updateOrder(order));
    }

    @GetMapping(value = "/getOrders", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> getOrders(){
        return orderService.getOrders();
    }

    @DeleteMapping(value = "/deleteOrder/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOrder(@PathVariable String orderId){
        orderService.deleteOrder(orderId);
    }
}
