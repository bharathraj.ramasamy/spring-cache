package com.saturn.springcache.repository;

import com.saturn.springcache.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String> {

}
